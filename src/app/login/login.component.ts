import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginCredentials } from '../login-credentials';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService
    ) { }

  loginForm: FormGroup;
  loginCredentials = new LoginCredentials('','');

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: new FormControl("", [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      password: new FormControl("", [
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z]).{8,}$")]),
    })
  }

  onLogin() {
    console.log(this.loginCredentials);
   }
}

